package sampPrePost;
import com.konylabs.middleware.common.DataPreProcessor2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Result;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;
public class SamplePre implements DataPreProcessor2
{
	private static final Logger LOGGER = Logger.getLogger(SamplePre.class);
	public boolean execute(HashMap arg0, DataControllerRequest arg1, DataControllerResponse arg2, Result arg3) throws Exception {
		LOGGER.info("BillPayment Composite Java Service Begins here");
		LOGGER.debug("Preprocessor 2");
		LOGGER.debug("Preprocessor" + arg0);
		LOGGER.debug("arg2" + arg2);
		String appver = (String) arg1.getParameter("konyreportingparams");
		String currLocale= (String) arg1.getParameter("konyreportingparams");
		LOGGER.debug("Preprocessor param appver" + appver);
		//OGGER.debug("Preprocessor param currLocale" + currLocale);
		Iterator<String> ite = arg1.getParameterNames();
		while (ite.hasNext()) {
			System.out.println("Preprocessor params" + (String)ite.next());
		}
		System.out.println("Preprocessor param" + arg1.getHeaderMap());
		System.out.println("Preprocessor" + arg2.getResponse());
		System.out.println("Preprocessor" + arg3);
		return true;
	}
}


