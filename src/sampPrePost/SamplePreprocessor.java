package sampPrePost;
import com.konylabs.middleware.common.DataPostProcessor2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.convertions.ResultToJSON;
import com.konylabs.middleware.dataobject.Dataset;
import com.konylabs.middleware.dataobject.Result;
import com.konylabs.middleware.session.Session;
import org.apache.log4j.Logger;
public class SamplePreprocessor implements DataPostProcessor2
{
	private static final Logger LOGGER = Logger.getLogger(SamplePreprocessor.class);
	public Object execute(Result arg0, DataControllerRequest arg1, DataControllerResponse arg2) throws Exception {
		System.out.println("argo" + ResultToJSON.convert(arg0));
		LOGGER.debug("argo.. " + arg0);
		Session session = arg1.getSession(false);
		session.setAttribute("mailSubject", "Content of Mail");
		Dataset baseDataset = arg0.findDataset("batters");
		System.out.println("ArrayList" + baseDataset);
		System.out.println("arg1header" + arg1.getHeaderMap());
		System.out.println("arg1appid" + arg1.getParameter("appID"));
		System.out.println("arg2" + arg2.getResponse());
		return arg0;
	}
}

