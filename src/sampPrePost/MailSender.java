package sampPrePost;

import org.apache.log4j.Logger;

import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Result;

public class MailSender {
	private static final Logger LOGGER = Logger.getLogger(MailSender.class);
	public Object invoke(String arg0, Object[] arg1, DataControllerRequest arg2, DataControllerResponse arg3) throws Exception {
		LOGGER.info("MailStarts");
		Result result = new Result();
		result.setParam(new Param("opstatus", "0", "int"));
		result.setParam(new Param("status", "Success", "string"));
		LOGGER.info("Mail successfully sent");
		return result;
		
	}
}
