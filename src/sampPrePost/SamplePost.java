package sampPrePost;
import com.konylabs.middleware.common.DataPostProcessor2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Dataset;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Record;
import com.konylabs.middleware.dataobject.Result;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

public class SamplePost implements DataPostProcessor2 {
	private static final Logger LOGGER = Logger.getLogger(SamplePost.class);
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://192.168.1.36:3306/sys";
	static final String USER = "root";
	static final String PASS = "admin_1234";
	
	@Override
	public Object execute(Result res, DataControllerRequest arg1, DataControllerResponse arg2) throws Exception {
		Connection conn = null;
		Statement stmt = null;
		try{
			
			Record rec = new Record();
			Dataset billerCatDS = new Dataset("databaseConn");
			ArrayList<Record> myValue = new ArrayList<Record>();
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL,USER,PASS);
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT id, first, last, age FROM Employees";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
				int id  = rs.getInt("id");
				int age = rs.getInt("age");
				String first = rs.getString("first");
				String last = rs.getString("last");
				rec.setParam(new Param("id", Integer.toString(id), "Int"));
				rec.setParam(new Param("age", Integer.toString(age), "Int"));
				rec.setParam(new Param("first", first, "String"));
				rec.setParam(new Param("last", last, "String"));
				myValue.add(rec);
				System.out.print("ID: " + id);
				System.out.print(", Age: " + age);
				System.out.print(", First: " + first);
				System.out.println(", Last: " + last);
			}
			rs.close();
			stmt.close();
			conn.close();
			billerCatDS.setRecords(myValue);
			res.setDataSet(billerCatDS);
			res.setParam(new Param("opstatus", "0", "String"));
			res.setParam(new Param("status", "success", "String"));
		}catch(SQLException se){
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return res;
	}
	
}
